import { invalidLogin, emailNotExists } from "./loginErrorMessages";
import { Request, Response } from "express";
import UsuarioBL from "../../../BL/UsuarioBL";

export const login = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  const erroresValidacion = obtenerErroresValidacion(email, password);
  if (erroresValidacion.length > 0) {
    return res.json({
      success: false,
      errors: erroresValidacion
    });
  }

  const usuarioBL = new UsuarioBL();

  const usuario = await usuarioBL.obtenerUsuarioPorEmail(email);
  if (!usuario) {
    return res.status(500).json(emailNotExistsResponse);
  }

  const esPasswordCorrecta = await usuarioBL.esPasswordCorrecta(
    password,
    usuario.password
  );

  if (!esPasswordCorrecta) {
    return res.status(500).json(invalidLoginResponse);
  }

  return res.json({
    success: true,
    errors: null,
    token: await usuarioBL.crearJWT(usuario)
  });
};

function obtenerErroresValidacion(email: string, password: string): any[] {
  const errores = [];

  if (!email) {
    errores.push({
      path: "email",
      message: "el campo email es requerido"
    });
  }

  if (!password) {
    errores.push({
      path: "password",
      message: "el campo password es requerido"
    });
  }

  return errores;
}

const invalidLoginResponse = {
  success: false,
  errors: [
    {
      path: "email",
      message: invalidLogin
    }
  ]
};

const emailNotExistsResponse = {
  success: false,
  errors: [
    {
      path: "email",
      message: emailNotExists
    }
  ]
};
