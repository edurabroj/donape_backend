import { emailDuplicated } from "./registerErrorMessages";
import { Request, Response } from "express";
import { formatYupError } from "../../../utils/formatYupError";
import UsuarioBL from "../../../BL/UsuarioBL";
import UsuarioBE from "../../../BE/UsuarioBE";
import { ValidationError } from "yup";

export const register = async (req: Request, res: Response) => {
  let usuario = obtenerUsuarioBE(req.body);
  const usuarioBL = new UsuarioBL();

  try {
    await usuarioBL.validarUsuario(usuario);
  } catch (errores) {
    return devolverErroresValidacion(res, errores);
  }

  const usuarioYaExiste = await usuarioBL.usuarioYaExiste(usuario);
  if (usuarioYaExiste) {
    return devolverUsuarioYaExiste(res);
  }

  usuario = await obtenerUsuarioPasswordCifrada(usuario);
  const usuarioRegistrado = await usuarioBL.crearUsuario(usuario);

  return await devolverRegistroExitoso(res, usuarioRegistrado);
};

async function obtenerUsuarioPasswordCifrada(
  usuario: UsuarioBE
): Promise<UsuarioBE> {
  const passwordCifrada = await new UsuarioBL().obtenerPasswordCifrada(
    usuario.password
  );
  usuario.password = passwordCifrada;
  return usuario;
}

function obtenerUsuarioBE(requestBody: any): UsuarioBE {
  const { email, password, nombre, celular, dni } = requestBody;

  return {
    id: 0,
    email,
    password,
    nombre,
    celular,
    dni
  };
}

function devolverErroresValidacion(
  res: Response,
  errores: ValidationError
): Response {
  return res.status(200).json({
    success: false,
    errors: formatYupError(errores)
  });
}

function devolverUsuarioYaExiste(res: Response): Response {
  return res.status(200).json({
    success: false,
    errors: [
      {
        path: "email",
        message: emailDuplicated
      }
    ]
  });
}

async function devolverRegistroExitoso(
  res: Response,
  usuario: UsuarioBE
): Promise<Response> {
  return res.json({
    success: true,
    errors: null,
    token: await new UsuarioBL().crearJWT(usuario)
  });
}
