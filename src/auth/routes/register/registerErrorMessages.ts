export const emailDuplicated = "Ya existe una cuenta registrada con este email";
export const emailTooShort = "El email es muy corto";
export const emailInvalid = "El email no es válido";
export const passwordTooShort = "La contraseña es muy corta";
export const nombreTooShort = "El nombre es muy corto";
export const celularInvalid = "El celular no es válido";
export const dniInvalid = "El DNI no es válido";
