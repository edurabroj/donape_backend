import NecesidadDA from "../DA/NecesidadDA";
import NecesidadBE from "../BE/NecesidadBE";

export default class NecesidadBL {
  necesidadDA: NecesidadDA;

  constructor(necesidadDA: NecesidadDA) {
    this.necesidadDA = necesidadDA;
  }

  async obtenerPorId(id: number): Promise<NecesidadBE> {
    return await this.necesidadDA.obtenerPorId(id);
  }

  public async obtenerNecesidadesPublicacion(
    publicacionId: number
  ): Promise<NecesidadBE[]> {
    return await this.necesidadDA.obtenerNecesidadesPublicacion(publicacionId);
  }
}
