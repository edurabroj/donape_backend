import UsuarioBE from "../BE/UsuarioBE";
import UsuarioDA from "../DA/UsuarioDA";
import * as bcrypt from "bcrypt";
import * as yup from "yup";
import * as jwt from "jsonwebtoken";
import {
  emailTooShort,
  emailInvalid,
  passwordTooShort,
  nombreTooShort,
  celularInvalid,
  dniInvalid
} from "../auth/routes/register/registerErrorMessages";

export default class UsuarioBL {
  public async validarUsuario(usuario: UsuarioBE): Promise<void> {
    const esquemaValidacion = this.obtenerEsquemaValidacion();
    await esquemaValidacion.validate(usuario, { abortEarly: false });
  }

  private obtenerEsquemaValidacion(): yup.ObjectSchema<{}> {
    return yup.object().shape({
      email: yup
        .string()
        .required()
        .min(3, emailTooShort)
        .max(255)
        .email(emailInvalid),
      password: yup
        .string()
        .required()
        .min(6, passwordTooShort)
        .max(255),
      nombre: yup
        .string()
        .required()
        .min(3, nombreTooShort)
        .max(255),
      celular: yup
        .string()
        .required()
        .min(9)
        .max(9)
        .matches(new RegExp("^9[0-9]{8}$"), celularInvalid),
      dni: yup
        .string()
        .required()
        .min(8, dniInvalid)
        .max(8)
        .matches(new RegExp("^[0-9]{8}$"), celularInvalid)
    });
  }

  public async usuarioYaExiste(usuario: UsuarioBE): Promise<boolean> {
    return new UsuarioDA().usuarioYaExiste(usuario);
  }

  public async obtenerPasswordCifrada(password: string): Promise<string> {
    return await bcrypt.hash(password, 10);
  }

  public async obtenerUsuarioPorEmail(
    email: string
  ): Promise<UsuarioBE | undefined> {
    return new UsuarioDA().obtenerUsuarioPorEmail(email);
  }

  public async esPasswordCorrecta(
    passwordIngresada: string,
    passwordCifrada: string
  ): Promise<boolean> {
    return await bcrypt.compare(passwordIngresada, passwordCifrada);
  }

  public async crearUsuario(usuario: UsuarioBE): Promise<UsuarioBE> {
    return new UsuarioDA().crearUsuario(usuario);
  }

  public async crearJWT(usuario: UsuarioBE) {
    const secret = process.env.SECRET as string;
    return jwt.sign({ id: usuario.id }, secret, { expiresIn: "5d" });
  }
}
