import { DonacionDA } from "../DA/DonacionDA";
import { DonacionCrearParams } from "../BE/Params/DonacionCrearParams";
import NecesidadDA from "../DA/NecesidadDA";

export class DonacionBL {
  donacionDA: DonacionDA;

  constructor(donacioDA: DonacionDA) {
    this.donacionDA = donacioDA;
  }

  async obtenerPorNecesidad(necesidadId: number) {
    return await this.donacionDA.obtenerPorNecesidad(necesidadId);
  }

  async obtenerPorUsuario(usuarioId: number) {
    return await this.donacionDA.obtenerPorUsuario(usuarioId);
  }

  async obtenerPorId(usuarioId: number) {
    return await this.donacionDA.obtenerPorId(usuarioId);
  }

  async crear(donacion: DonacionCrearParams) {
    if (!donacion.articuloDonado) {
      const necesidad = await new NecesidadDA().obtenerPorId(
        donacion.necesidad
      );
      donacion.articuloDonado = necesidad.articulo;
    }
    const donacionCreada = await this.donacionDA.crear(donacion);

    this.donacionDA.establecerEnEstadoPendiente(donacionCreada);

    return donacionCreada;
  }
}
