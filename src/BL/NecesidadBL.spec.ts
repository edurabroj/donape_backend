import * as chai from "chai";
const expect = chai.expect;
import * as sinon from "sinon";
import * as sinonChai from "sinon-chai";
import NecesidadBL from "./NecesidadBL";
import NecesidadDA from "../DA/NecesidadDA";

chai.use(sinonChai);

describe("NecesidadBL", function() {
  describe("obtener necesidades por donacion", function() {
    it("usa el método obtenerNecesidadesPublicacion de NecesidadDA", function() {
      const necesidadDA = sinon.createStubInstance(NecesidadDA);

      const necesidadBL = new NecesidadBL(necesidadDA);
      necesidadBL.obtenerNecesidadesPublicacion(1);

      expect(necesidadDA.obtenerNecesidadesPublicacion).to.have.been.calledWith(
        1
      );
    });
  });
});
