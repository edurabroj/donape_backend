import PublicacionDA from "../DA/PublicacionDA";
import PublicacionBE from "../BE/PublicacionBE";

export default class PublicacionBL {
  publicacionDA: PublicacionDA;

  constructor(publicacionDA: PublicacionDA) {
    this.publicacionDA = publicacionDA;
  }

  async obtenerPorId(id: number): Promise<PublicacionBE> {
    return await this.publicacionDA.obtenerPorId(id);
  }

  async obtenerPublicaciones(): Promise<PublicacionBE[]> {
    return await this.publicacionDA.obtenerPublicaciones();
  }
}
