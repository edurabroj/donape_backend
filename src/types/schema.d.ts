// tslint:disable
// graphql typescript definitions

export namespace GQL {
interface IGraphQLResponseRoot {
data?: IQuery | IMutation;
errors?: Array<IGraphQLResponseError>;
}

interface IGraphQLResponseError {
/** Required for all errors */
message: string;
locations?: Array<IGraphQLResponseErrorLocation>;
/** 7.2.2 says 'GraphQL servers may provide additional entries to error' */
[propName: string]: any;
}

interface IGraphQLResponseErrorLocation {
line: number;
column: number;
}

interface IQuery {
__typename: "Query";
donacionesByNecesidad: Array<IDonacion | null>;
donacionesByUsuario: Array<IDonacion | null>;
donacion: IDonacion | null;
necesidad: INecesidad | null;
publicaciones: Array<IPublicacion | null>;
publicacion: IPublicacion | null;
}

interface IDonacionesByNecesidadOnQueryArguments {
necesidadId: number;
}

interface IDonacionOnQueryArguments {
id: number;
}

interface INecesidadOnQueryArguments {
id: number;
}

interface IPublicacionOnQueryArguments {
id: number;
}

interface IDonacion {
__typename: "Donacion";
id: number;
necesidad: INecesidad | null;
fecha: any;
cantidad: number;
estados: Array<IDonacionEstado | null> | null;
}

interface INecesidad {
__typename: "Necesidad";
id: number;
articulo: string;
cantidadRequerida: number;
cantidadRecolectada: number;
cantidadFaltante: number;
fecha: any;
publicacion: IPublicacion | null;
donaciones: Array<IDonacion | null> | null;
}

interface IPublicacion {
__typename: "Publicacion";
id: number;
titulo: string;
descripcion: string;
fecha: any;
necesidades: Array<INecesidad | null> | null;
imagenes: Array<IImagen | null> | null;
}

interface IImagen {
__typename: "Imagen";
id: number;
url: string;
}

interface IDonacionEstado {
__typename: "DonacionEstado";
id: string | null;
donacion: IDonacion | null;
estado: IEstado | null;
fecha: any | null;
imagenes: Array<IImagen | null> | null;
}

interface IEstado {
__typename: "Estado";
id: string | null;
nombre: string | null;
actualizaNecesidad: boolean | null;
}

interface IMutation {
__typename: "Mutation";
crearDonacion: IDonacion | null;
}

interface ICrearDonacionOnMutationArguments {
necesidad: number;
cantidad: number;
articuloDonado?: string | null;
}
}

// tslint:enable
