import { GraphQLScalarType, Kind, ValueNode } from "graphql";
import * as moment from "moment";

export const fecha = new GraphQLScalarType({
  name: "Fecha",
  description: "Fecha personalizada",
  parseValue(value) {
    return Date.parse(value);
  },
  serialize(value) {
    return moment(value).format("DD/MM/YYYY");
  },
  parseLiteral(ast: ValueNode) {
    if (ast.kind === Kind.INT) {
      return new Date(ast.value);
    }
    return null;
  }
});
