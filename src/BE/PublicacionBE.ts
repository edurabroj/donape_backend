import NecesidadBE from "./NecesidadBE";
import UsuarioBE from "./UsuarioBE";
import ImagenPublicacionBE from "./ImagenPublicacionBE";

export default interface PublicacionBE {
  id: number;
  usuarioId: number;
  titulo: string;
  descripcion: string;
  fecha: Date;
  necesidades: NecesidadBE[];
  usuario: UsuarioBE;
  imagenes: ImagenPublicacionBE[];
}
