export default interface UsuarioBE {
  id: number;
  email: string;
  password: string;
  nombre: string;
  celular: string;
  dni: string;
}
