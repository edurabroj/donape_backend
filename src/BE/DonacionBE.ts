import DonacionEstadoBE from "./DonacionEstadoBE";
import UsuarioBE from "./UsuarioBE";
import NecesidadBE from "./NecesidadBE";

export default interface DonacionBE {
  id: number;
  articuloDonado: string;
  fecha: Date;
  cantidad: number;
  estados: DonacionEstadoBE[];
  necesidad: NecesidadBE;
  usuario: UsuarioBE;
}
