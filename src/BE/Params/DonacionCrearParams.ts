export interface DonacionCrearParams {
  articuloDonado: string | undefined | null;
  necesidad: number;
  cantidad: number;
  usuario: number;
}
