import DonacionEstadoBE from "./DonacionEstadoBE";

export default interface ImagenEstadoBE {
  id: number;
  nombre: string;
  url: string;
  donacionEstados: DonacionEstadoBE[];
}
