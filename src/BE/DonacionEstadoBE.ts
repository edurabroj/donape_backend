import DonacionBE from "./DonacionBE";
import ImagenEstadoBE from "./ImagenEstadoBE";
import EstadoBE from "./EstadoBE";

export default interface DonacionEstadoBE {
  id: number;
  fecha: Date;
  imagenes: ImagenEstadoBE[];
  donacion: DonacionBE;
  estado: EstadoBE;
}
