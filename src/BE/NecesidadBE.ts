import PublicacionBE from "./PublicacionBE";
import DonacionBE from "./DonacionBE";

export default interface NecesidadBE {
  id: number;
  articulo: string;
  esIlimitada: boolean;
  cantidadRequerida: number;
  cantidadRecolectada: number;
  cantidadFaltante: number;
  fecha: Date;
  estaSatisfecha: boolean;
  publicacion: PublicacionBE;
  donaciones: DonacionBE[];
}
