import DonacionEstadoBE from "./DonacionEstadoBE";

export default interface EstadoBE {
  id: string;
  nombre: string;
  actualizaNecesidad: boolean;
  donaciones: DonacionEstadoBE[];
}
