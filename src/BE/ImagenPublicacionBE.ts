import PublicacionBE from "./PublicacionBE";

export default interface ImagenPublicacionBE {
  id: number;
  url: string;
  publicacion: PublicacionBE;
}
