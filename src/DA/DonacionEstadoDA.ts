import DonacionEstadoBE from "../BE/DonacionEstadoBE";
import { DonacionEstado } from "../database/entity/DonacionEstado";
import DonacionBE from "../BE/DonacionBE";
import EstadoBE from "../BE/EstadoBE";

export class DonacionEstadoDA {
  async obtenerEstadosDonacion(
    donacionId: number
  ): Promise<DonacionEstadoBE[]> {
    return await DonacionEstado.find({
      where: { donacion: donacionId },
      relations: ["estado", "imagenes"]
    });
  }

  async crearDonacionEstado(
    donacion: DonacionBE,
    estado: EstadoBE
  ): Promise<DonacionEstadoBE> {
    return await DonacionEstado.create({
      donacion,
      estado
    }).save();
  }
}
