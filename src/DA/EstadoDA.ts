import EstadoBE from "../BE/EstadoBE";
import { Estado } from "../database/entity/Estado";
import { ObjetoNoExisteError } from "../Errores/ObjetoNoExisteError";

export class EstadoDA {
  async obtenerPorId(id: string): Promise<EstadoBE> {
    const estado = await Estado.findOne({ where: { id } });
    if (!estado) {
      throw new ObjetoNoExisteError();
    } else {
      return estado;
    }
  }
}
