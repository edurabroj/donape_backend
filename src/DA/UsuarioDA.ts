import UsuarioBE from "../BE/UsuarioBE";
import { Usuario } from "../database/entity/Usuario";

export default class UsuarioDA {
  public async usuarioYaExiste(usuario: UsuarioBE): Promise<boolean> {
    return (
      (await Usuario.findOne({
        where: { email: usuario.email }
      })) != null
    );
  }

  public async crearUsuario(usuario: UsuarioBE): Promise<UsuarioBE> {
    const usuarioBD = await Usuario.create({
      ...usuario
    }).save();

    return this.obtenerUsuarioBE(usuarioBD) as UsuarioBE;
  }

  public async obtenerUsuarioPorEmail(
    email: string
  ): Promise<UsuarioBE | undefined> {
    const usuarioBD = await Usuario.findOne({ where: { email } });
    return this.obtenerUsuarioBE(usuarioBD);
  }

  private obtenerUsuarioBE(
    usuarioBD: Usuario | undefined
  ): UsuarioBE | undefined {
    if (!usuarioBD) {
      return undefined;
    }
    return {
      id: usuarioBD.id,
      email: usuarioBD.email,
      password: usuarioBD.password,
      nombre: usuarioBD.nombre,
      celular: usuarioBD.celular,
      dni: usuarioBD.dni
    };
  }
}
