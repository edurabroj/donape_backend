import { Donacion } from "../database/entity/Donacion";
import DonacionBE from "../BE/DonacionBE";
import { Utilidades } from "../utils/Utilidades";
import { ObjetoNoExisteError } from "../Errores/ObjetoNoExisteError";
import { DonacionEstadoDA } from "./DonacionEstadoDA";
import { DonacionCrearParams } from "../BE/Params/DonacionCrearParams";
import { EstadoDA } from "./EstadoDA";

export class DonacionDA {
  async obtenerPorUsuario(usuarioId: number): Promise<DonacionBE[]> {
    const donaciones = await Donacion.find({
      where: { usuario: usuarioId },
      relations: ["necesidad", "necesidad.publicacion"]
    });
    return await this.obtenerDonacionesCompletas(donaciones);
  }

  async obtenerDonacionesConfirmadasPorNecesidad(
    necesidadId: number
  ): Promise<DonacionBE[]> {
    const donacionesNecesidad = await this.obtenerPorNecesidad(necesidadId);

    const donacionesConfirmadas: DonacionBE[] = await this.obtenerDonacionesConfirmadas(
      donacionesNecesidad
    );

    return await this.obtenerDonacionesCompletas(donacionesConfirmadas);
  }

  async obtenerPorNecesidad(necesidadId: number): Promise<DonacionBE[]> {
    const donaciones = await Donacion.find({
      where: { necesidad: necesidadId }
    });
    return await this.obtenerDonacionesCompletas(donaciones);
  }

  async obtenerDonacionesConfirmadas(
    donaciones: DonacionBE[]
  ): Promise<DonacionBE[]> {
    return await new Utilidades().filterAsync(
      donaciones,
      async (donacion: any) => {
        const estadosDonacion = await new DonacionEstadoDA().obtenerEstadosDonacion(
          donacion.id
        );

        const tieneEstadoConfirmado = estadosDonacion.filter(
          donacionEstado => donacionEstado.estado.actualizaNecesidad
        );

        return tieneEstadoConfirmado.length > 0;
      }
    );
  }

  async obtenerDonacionesCompletas(
    donaciones: DonacionBE[]
  ): Promise<DonacionBE[]> {
    return Promise.all(
      donaciones.map(
        async donacion => await this.obtenerDonacionCompleta(donacion)
      )
    );
  }

  async obtenerPorId(id: number): Promise<DonacionBE> {
    const donacion = await Donacion.findOne({ where: { id } });
    if (!donacion) {
      throw new ObjetoNoExisteError();
    } else {
      return await this.obtenerDonacionCompleta(donacion);
    }
  }

  async obtenerDonacionCompleta(donacion: DonacionBE): Promise<DonacionBE> {
    return {
      ...donacion,
      estados: await new DonacionEstadoDA().obtenerEstadosDonacion(donacion.id),
      cantidad: +donacion.cantidad
    };
  }

  async crear(donacion: DonacionCrearParams): Promise<DonacionBE> {
    return await Donacion.create({
      ...donacion,
      articuloDonado: donacion.articuloDonado as string,
      necesidad: { id: donacion.necesidad },
      usuario: { id: donacion.usuario }
    }).save();
  }

  async establecerEnEstadoPendiente(donacion: DonacionBE) {
    return await new DonacionEstadoDA().crearDonacionEstado(
      donacion,
      await new EstadoDA().obtenerPorId("PN")
    );
  }
}
