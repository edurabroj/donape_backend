import NecesidadBE from "../BE/NecesidadBE";
import { Necesidad } from "../database/entity/Necesidad";
import { ObjetoNoExisteError } from "../Errores/ObjetoNoExisteError";
import { DonacionDA } from "./DonacionDA";

export default class NecesidadDA {
  relations: string[];

  constructor() {
    this.relations = ["publicacion"];
  }

  public async obtenerNecesidadesPublicacion(
    publicacionId: number
  ): Promise<NecesidadBE[]> {
    const necesidades: NecesidadBE[] = await Necesidad.find({
      where: { publicacion: publicacionId },
      relations: this.relations
    });

    const necesidadesCompletas: NecesidadBE[] = await Promise.all(
      necesidades.map(
        async necesidad => await this.obtenerNecesidadCompleta(necesidad)
      )
    );

    return necesidadesCompletas;
  }

  async obtenerPorId(id: number): Promise<NecesidadBE> {
    const necesidad = await Necesidad.findOne({
      where: { id },
      relations: this.relations
    });

    if (!necesidad) {
      throw new ObjetoNoExisteError();
    } else {
      return this.obtenerNecesidadCompleta(necesidad);
    }
  }

  async obtenerNecesidadCompleta(necesidad: NecesidadBE): Promise<NecesidadBE> {
    const cantidadRecolectada = await this.obtenerCantidadRecolectada(
      necesidad.id
    );
    return {
      ...necesidad,
      cantidadRecolectada,
      cantidadFaltante: necesidad.cantidadRequerida - cantidadRecolectada,
      donaciones: await new DonacionDA().obtenerPorNecesidad(necesidad.id)
    };
  }

  async obtenerCantidadRecolectada(necesidadId: number): Promise<number> {
    const donacionesConfirmadas = await new DonacionDA().obtenerDonacionesConfirmadasPorNecesidad(
      necesidadId
    );

    const cantidadesDonacionesConfirmadas = donacionesConfirmadas.map(
      donacion => donacion.cantidad as number
    );

    const sumaCantidades = cantidadesDonacionesConfirmadas.reduce(
      (anterior: number, actual: number) => anterior + actual,
      0
    );

    return sumaCantidades;
  }
}
