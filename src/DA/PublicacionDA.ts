import PublicacionBE from "../BE/PublicacionBE";
import { Publicacion } from "../database/entity/Publicacion";
import { ObjetoNoExisteError } from "../Errores/ObjetoNoExisteError";
import NecesidadDA from "./NecesidadDA";

export default class PublicacionDA {
  relations: string[];

  constructor() {
    this.relations = ["imagenes"];
  }

  async obtenerPublicaciones(): Promise<PublicacionBE[]> {
    const publicaciones = await Publicacion.find({ relations: this.relations });
    return await this.obtenerPublicacionesCompletas(publicaciones);
  }

  async obtenerPorId(publicacionId: number): Promise<PublicacionBE> {
    const publicacion = await Publicacion.findOne({
      where: { id: publicacionId },
      relations: this.relations
    });

    if (publicacion === undefined) {
      throw new ObjetoNoExisteError();
    }

    return await this.obtenerPublicacionCompleta(publicacion);
  }

  async obtenerPublicacionesCompletas(
    publicaciones: PublicacionBE[]
  ): Promise<PublicacionBE[]> {
    return await Promise.all(
      publicaciones.map(
        async (publicacion): Promise<PublicacionBE> => {
          return await this.obtenerPublicacionCompleta(publicacion);
        }
      )
    );
  }

  async obtenerPublicacionCompleta(
    publicacion: PublicacionBE
  ): Promise<PublicacionBE> {
    return {
      ...publicacion,
      necesidades: await new NecesidadDA().obtenerNecesidadesPublicacion(
        publicacion.id
      )
    };
  }
}
