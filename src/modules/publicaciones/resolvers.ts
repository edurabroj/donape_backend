import { GQL } from "../../types/schema";
import PublicacionBE from "../../BE/PublicacionBE";
import PublicacionDA from "../../DA/PublicacionDA";
import PublicacionBL from "../../BL/PublicacionBL";
import { fecha } from "../../types/fecha";

export const resolvers = {
  Fecha: fecha,
  Query: {
    publicaciones: async () => {
      return await new PublicacionBL(
        new PublicacionDA()
      ).obtenerPublicaciones();
    },
    publicacion: async (
      // tslint:disable-next-line:variable-name
      _parent: any,
      { id }: GQL.IPublicacionOnQueryArguments
    ): Promise<PublicacionBE> => {
      const publicacion = await new PublicacionBL(
        new PublicacionDA()
      ).obtenerPorId(id);
      return publicacion;
    }
  }
};
