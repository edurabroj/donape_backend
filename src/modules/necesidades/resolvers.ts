import { GQL } from "../../types/schema";
import NecesidadBL from "../../BL/NecesidadBL";
import NecesidadDA from "../../DA/NecesidadDA";
import { fecha } from "../../types/fecha";

export const resolvers = {
  Fecha: fecha,
  Query: {
    // tslint:disable-next-line:variable-name
    necesidad: async (
      // tslint:disable-next-line:variable-name
      _parametros: any,
      { id }: GQL.INecesidadOnQueryArguments
    ) => {
      return await new NecesidadBL(new NecesidadDA()).obtenerPorId(id);
    }
  }
};
