import { GQL } from "../../types/schema";
import { checkAuth } from "../../auth/checkAuth";
import { DonacionBL } from "../../BL/DonacionBL";
import { DonacionDA } from "../../DA/DonacionDA";
import { fecha } from "../../types/fecha";
const donacionBL = new DonacionBL(new DonacionDA());

export const resolvers = {
  Fecha: fecha,
  Query: {
    donacionesByNecesidad: async (
      // tslint:disable-next-line:variable-name
      _parent: any,
      { necesidadId }: GQL.IDonacionesByNecesidadOnQueryArguments
    ) => {
      return await donacionBL.obtenerPorNecesidad(necesidadId);
    },

    // tslint:disable-next-line:variable-name
    donacionesByUsuario: async (_parent: any, _params: any, { user }: any) => {
      checkAuth(user);
      return await donacionBL.obtenerPorUsuario(user.id);
    },

    // tslint:disable-next-line:variable-name
    donacion: async (_parent: any, { id }: GQL.IDonacionOnQueryArguments) => {
      return await donacionBL.obtenerPorId(id);
    }
  },
  Mutation: {
    crearDonacion: async (
      // tslint:disable-next-line:variable-name
      _parent: any,
      {
        necesidad,
        cantidad,
        articuloDonado
      }: GQL.ICrearDonacionOnMutationArguments,
      { user }: any
    ) => {
      checkAuth(user);

      return await donacionBL.crear({
        articuloDonado,
        necesidad,
        cantidad,
        usuario: user.id
      });
    }
  }
};
