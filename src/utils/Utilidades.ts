export class Utilidades {
  async filterAsync(arr: any[], callback: any) {
    const fail = Symbol();
    return (await Promise.all(
      arr.map(async item => ((await callback(item)) ? item : fail))
    )).filter(item => item !== fail);
  }
}
