import * as fs from "fs";
import { importSchema } from "graphql-import";
import { makeExecutableSchema, mergeSchemas } from "graphql-tools";
import * as path from "path";

export const genSchema = () => {
  const folders = fs.readdirSync(path.join(__dirname, "../modules"));

  const schemas: any[] = folders.map(folder => {
    const { resolvers } = require(`../modules/${folder}/resolvers`);
    const typeDefs = importSchema(
      path.join(__dirname, `../modules/${folder}/schema.graphql`)
    );
    return makeExecutableSchema({ resolvers, typeDefs });
  });

  return mergeSchemas({ schemas });
};
