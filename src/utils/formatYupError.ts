import { ValidationError } from "yup";

interface ErrorYup {
  path: string;
  message: string;
}

export const formatYupError = (err: ValidationError): ErrorYup[] => {
  return err.inner.map(error => {
    return {
      path: error.path,
      message: error.message
    };
  });
};
