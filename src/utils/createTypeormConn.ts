import { createConnection, getConnectionOptions } from "typeorm";

export const createTypeormConn = async () => {
  const connectionOptions = await getConnectionOptions();
  return createConnection({
    ...connectionOptions,
    entities: ["src/database/entity/**/*.ts"],
    migrations: ["src/database/migration/**/*.ts"],
    subscribers: ["src/database/subscriber/**/*.ts"]
  });
};
