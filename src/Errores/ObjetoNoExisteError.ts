export class ObjetoNoExisteError extends Error {
  public message: string;

  constructor() {
    super();

    Object.setPrototypeOf(this, ObjetoNoExisteError.prototype);
    this.message = "El objeto solicitado no existe";
  }
}
